provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Environment = var.environment
      Owner       = "infra"
      Project     = "conxai-infra"
      CreatedBy   = "terraform"
    }
  }
}

#data "aws_eks_cluster" "cluster" {
#  name = format("%s-%s", var.environment, var.cluster_name)
#}

locals {
  cluster_host = trim(var.cluster_endpoint, "")
  cluster_ca   = base64decode(var.cluster_certificate_authority_data)
  cluster_name = var.cluster_name
}


provider "kubernetes" {
  host                   = local.cluster_host
  cluster_ca_certificate = local.cluster_ca
  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    args        = ["eks", "get-token", "--cluster-name", local.cluster_name]
    command     = "aws"
  }
}

provider "helm" {
  kubernetes {
    host                   = local.cluster_host
    cluster_ca_certificate = local.cluster_ca
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      args        = ["eks", "get-token", "--cluster-name", local.cluster_name]
      command     = "aws"
    }
  }
}

provider "kubectl" {
  load_config_file       = false
  host                   = local.cluster_host
  cluster_ca_certificate = local.cluster_ca
  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    args        = ["eks", "get-token", "--cluster-name", local.cluster_name]
    command     = "aws"
  }
}
