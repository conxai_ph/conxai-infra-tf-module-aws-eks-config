locals {
  eks_oidc_issuer_url = var.cluster_oidc_issuer_url
  addon_context = {
    aws_caller_identity_account_id = data.aws_caller_identity.current.account_id
    aws_caller_identity_arn        = data.aws_caller_identity.current.arn
    aws_eks_cluster_endpoint       = local.cluster_host
    aws_partition_id               = data.aws_partition.current.partition
    aws_region_name                = var.region
    eks_cluster_id                 = local.cluster_name
    eks_oidc_issuer_url            = local.eks_oidc_issuer_url
    eks_oidc_provider_arn          = "arn:${data.aws_partition.current.partition}:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${local.eks_oidc_issuer_url}"
    tags                           = var.tags
    irsa_iam_role_path             = "/eks/"
    irsa_iam_permissions_boundary  = ""
    default_repository             = "602401143452.dkr.ecr.eu-central-1.amazonaws.com"
  }
}

# AWS Load Balancer Controller - https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.6/
module "aws_load_balancer_controller" {
  count  = 1
  source = "./addons/aws-load-balancer-controller"
  helm_config = {
    service_account                     = "aws-lb-sa"
    enable_aws_load_balancer_controller = true
  }
  manage_via_gitops = false
  addon_context     = local.addon_context
}

## GPU Device Manager - Deployed as daemon set
resource "kubectl_manifest" "gpu-device-manager" {
  yaml_body = file("${path.module}/templates/device-plugin.yml")
}

## AWS Fluent Bit - https://github.com/aws/aws-for-fluent-bit
module "aws_for_fluent_bit" {
  count                    = true ? 1 : 0
  source                   = "./addons/aws-for-fluentbit"
  helm_config              = {}
  irsa_policies            = []
  create_cw_log_group      = true
  cw_log_group_name        = null
  cw_log_group_retention   = 90
  cw_log_group_kms_key_arn = null
  manage_via_gitops        = false
  addon_context            = local.addon_context
}
