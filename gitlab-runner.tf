########################################################################################################################
# Description: Deploy a gitlab runner to the EKS Cluster
########################################################################################################################
resource "helm_release" "gitlab-runner" {
  name             = format("%s-%s-gitlab-runner", var.environment, var.cluster_name)
  repository       = "https://charts.gitlab.io"
  chart            = "gitlab-runner"
  namespace        = "gitlab"
  create_namespace = true
  values           = [file("${path.module}/helm-charts/gitlab-runner.yaml")]
  timeout          = 1800
  force_update     = false
}
########################################################################################################################