#module "eks_config" {
#  source                             = "../"
#  region                             = "eu-central-1"
#  cluster_name                       = data.aws_ssm_parameter.cluster_id.value #module.eks.cluster_name
#  environment                        = "dev"
#  cluster_oidc_issuer_url            = data.aws_ssm_parameter.cluster_oidc_issuer_url.value #module.eks.cluster_oidc_issuer_url
#  cluster_endpoint                   = data.aws_ssm_parameter.cluster_endpoint.value        #module.eks.cluster_endpoint
#  cluster_certificate_authority_data = data.aws_ssm_parameter.cluster_ca.value              #module.eks.cluster_certificate_authority_data
#}

module "eks_config" {
  source                             = "../"
  region                             = "eu-central-1"
  cluster_name                       = module.eks.cluster_name
  environment                        = "dev"
  cluster_oidc_issuer_url            = module.eks.cluster_oidc_issuer_url
  cluster_endpoint                   = module.eks.cluster_endpoint
  cluster_certificate_authority_data = module.eks.cluster_certificate_authority_data
}