locals {
  cluster_name = "test-eks-config-6"
}

module "eks" {
  source                  = "git::https://gitlab.com/consai/products/infrastructure/conxai-infra-tf-module-aws-eks.git?ref=3.0.5318843876"
  region                  = "eu-central-1"
  environment             = "dev"
  cluster_version         = "1.24"
  cluster_name            = local.cluster_name
  ssh_key_s3_bucket       = "terraform-state-038562570677"
  ssh_key_s3_path         = "tftest/eks"
  ssh_key_bastion_s3_path = "tftest/eksbastion"
  tags                    = {}
}


