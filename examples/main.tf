terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
  backend "s3" {}
}
#
#provider "aws" {
#  region = "eu-central-1"
#  default_tags {
#    tags = {
#      Environment = "dev"
#      Owner       = "infra"
#      Project     = "conxai-infra"
#      CreatedBy   = "terraform"
#    }
#  }
#}
#
#data "aws_caller_identity" "current" {}
#
#data "aws_eks_cluster" "cluster" {
#  name = jsondecode(var.cluster_name)
#}
#
#locals {
#  cluster_host = data.aws_eks_cluster.cluster.endpoint
#  cluster_ca   = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
#  cluster_name = data.aws_eks_cluster.cluster.id
#}
#
#
#provider "kubernetes" {
#  host                   = local.cluster_host
#  cluster_ca_certificate = local.cluster_ca
#  exec {
#    api_version = "client.authentication.k8s.io/v1beta1"
#    args        = ["eks", "get-token", "--cluster-name", local.cluster_name]a
#    command     = "aws"
#  }
#}
#
#provider "helm" {
#  kubernetes {
#    host                   = local.cluster_host
#    cluster_ca_certificate = local.cluster_ca
#    exec {
#      api_version = "client.authentication.k8s.io/v1beta1"
#      args        = ["eks", "get-token", "--cluster-name", local.cluster_name]
#      command     = "aws"
#    }
#  }
#}
#
#provider "kubectl" {
#  load_config_file       = false
#  host                   = local.cluster_host
#  cluster_ca_certificate = local.cluster_ca
#  exec {
#    api_version = "client.authentication.k8s.io/v1beta1"
#    args        = ["eks", "get-token", "--cluster-name", local.cluster_name]
#    command     = "aws"
#  }
#}
