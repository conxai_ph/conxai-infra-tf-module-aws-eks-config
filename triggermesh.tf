########################################################################################################################
# Description: Triggermesh
########################################################################################################################
resource "aws_iam_role" "triggermesh_controller_iam_role" {
  assume_role_policy  = data.aws_iam_policy_document.triggermesh_controller_iam_assume_role_policy.json
  managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonS3FullAccess", "arn:aws:iam::aws:policy/AmazonSQSFullAccess"]
}

data "aws_iam_policy_document" "triggermesh_controller_iam_assume_role_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [local.addon_context.eks_oidc_provider_arn]
    }
    condition {
      test     = "StringEquals"
      variable = "${local.addon_context.eks_oidc_issuer_url}:aud"

      values = ["sts.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      variable = "${local.addon_context.eks_oidc_issuer_url}:sub"

      values = ["system:serviceaccount:triggermesh:conxai-soc-triggermesh-controller"]
    }
  }
}

resource "helm_release" "triggermesh" {
  name             = format("%s-%s-triggermesh", var.environment, var.cluster_name)
  repository       = "https://storage.googleapis.com/triggermesh-charts"
  chart            = "triggermesh"
  namespace        = "triggermesh"
  create_namespace = true
  values           = [templatefile("${path.module}/helm-charts/triggermesh-config.tftpl", { aws_iam_role_arn = aws_iam_role.triggermesh_controller_iam_role.arn })]
  timeout          = 1800
  force_update     = false
}
########################################################################################################################