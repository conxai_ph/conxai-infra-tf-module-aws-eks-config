########################################################################################################################
# Description: EKS Autoscaler
########################################################################################################################
resource "aws_iam_role" "autoscaler_iam_role" {
  assume_role_policy = data.aws_iam_policy_document.autoscaler_iam_assume_role_policy.json
  inline_policy {
    policy = data.aws_iam_policy_document.autoscaler_iam_policy.json
  }
}

data "aws_iam_policy_document" "autoscaler_iam_policy" {
  statement {
    actions = [
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeAutoScalingInstances",
      "autoscaling:DescribeLaunchConfigurations",
      "autoscaling:DescribeScalingActivities",
      "autoscaling:DescribeTags",
      "ec2:DescribeInstanceTypes",
      "ec2:DescribeLaunchTemplateVersions"
    ]
    resources = ["*"]
  }
  statement {
    actions = [
      "autoscaling:SetDesiredCapacity",
      "autoscaling:TerminateInstanceInAutoScalingGroup",
      "ec2:DescribeImages",
      "ec2:GetInstanceTypesFromInstanceRequirements",
      "eks:DescribeNodegroup"
    ]
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "autoscaler_iam_assume_role_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [local.addon_context.eks_oidc_provider_arn]
    }
    condition {
      test     = "StringEquals"
      variable = "${local.addon_context.eks_oidc_issuer_url}:aud"

      values = ["sts.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      variable = "${local.addon_context.eks_oidc_issuer_url}:sub"

      values = ["system:serviceaccount:kube-system:autoscaler-aws-cluster-autoscaler"]
    }
  }
}

resource "helm_release" "autoscaler" {
  name             = format("%s-%s-autoscaler", var.environment, var.cluster_name)
  repository       = "https://kubernetes.github.io/autoscaler"
  chart            = "cluster-autoscaler"
  namespace        = "kube-system"
  create_namespace = true
  values           = [templatefile("${path.module}/helm-charts/autoscaler-config.tftpl", { clusterName = format("%s-%s", var.environment, var.cluster_name), region = var.region, aws_iam_role_arn = aws_iam_role.autoscaler_iam_role.arn })]
  timeout          = 1800
  force_update     = false
}
########################################################################################################################